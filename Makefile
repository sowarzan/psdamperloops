CXX=/acc/local/Linux/x86_64-linux-gcc/current/gcc/bin/g++

LIBPSDL_VER=1.0.0

CPU?=L867

EDGE_VER=2.1

EDGE_DRIVER_PATH=/acc/local/$(CPU)/drv/edge/$(EDGE_VER)

# Path to current directory
export PROJ_DIR=$(abspath $(PWD))

LIBPSDL_PATH?=$(PROJ_DIR)/../../MemMap/libs/rf-ps_damper-lib/build
LIBADC_PATH?=$(PROJ_DIR)/../../MemMap/libs/rf-adc_ctrl-lib/build
LIBCLKS_PATH?=$(PROJ_DIR)/../../MemMap/libs/rf-clks_ctrl-lib/build
LIBFP_PATH?=$(PROJ_DIR)/../../MemMap/libs/rf-fpintfce_ctrl-lib/build
LIBDAC_PATH?=$(PROJ_DIR)/../../MemMap/libs/rf-dac_ctrl-lib/build
LIBUSR_PATH?=$(PROJ_DIR)/../../MemMap/libs/rf-user_logic-lib/build

SRC_PATH=src

INCLUDES= -I$(LIBPSDL_PATH)/include
INCLUDES+= -I$(LIBADC_PATH)/include
INCLUDES+= -I$(LIBCLKS_PATH)/include
INCLUDES+= -I$(LIBFP_PATH)/include
INCLUDES+= -I$(LIBDAC_PATH)/include
INCLUDES+= -I$(LIBUSR_PATH)/include
INCLUDES+= -I$(EDGE_DRIVER_PATH)/include
INCLUDES+= -I$(SRC_PATH)

CXXFLAGS= -std=c++14 -g $(INCLUDES) -D_GLIBCXX_USE_CXX11_ABI=0 --sysroot=/acc/sys/L867 --sysroot=/acc/sys/L867 -m64 -fPIC -Wl,-rpath=/acc/sys/Linux/toolchain_libs:/acc/local/Linux/x86_64-linux-gcc/toolchain_libs:$(EDGE_DRIVER_PATH)/lib

LDFLAGS= -L$(LIBPSDL_PATH)/lib/$(CPU) -lrf-ps_damper-lib
LDFLAGS+= -L$(LIBADC_PATH)/lib/$(CPU) -lrf-adc_ctrl-lib
LDFLAGS+= -L$(LIBCLKS_PATH)/lib/$(CPU) -lrf-clks_ctrl-lib
LDFLAGS+= -L$(LIBFP_PATH)/lib/$(CPU) -lrf-fpintfce_ctrl-lib
LDFLAGS+= -L$(LIBDAC_PATH)/lib/$(CPU) -lrf-dac_ctrl-lib
LDFLAGS+= -L$(LIBUSR_PATH)/lib/$(CPU) -lrf-user_logic-lib
LDFLAGS+= -L$(EDGE_DRIVER_PATH)/lib -ledge

TARGET=psdamperloop

BUILD_DIR=build


SOURCES:=$(SRC_PATH)/main.cpp
SOURCES+=$(SRC_PATH)/LTC218XDriver.cpp
SOURCES+=$(SRC_PATH)/ADCDriver.cpp
SOURCES+=$(SRC_PATH)/AD56XXDriver.cpp
SOURCES+=$(SRC_PATH)/AD9512Driver.cpp

OBJECTS=$(SOURCES:$(SRC_PATH)/%.cpp=$(BUILD_DIR)/%.o)

all: build_dir $(TARGET)

$(BUILD_DIR)/%.o: $(SRC_PATH)/%.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@

$(TARGET): $(OBJECTS)
	$(CXX) $(CXXFLAGS) -o $(BUILD_DIR)/$@ $^ $(LDFLAGS)

build_dir:
	mkdir -p $(BUILD_DIR)

clean:
	rm -r $(BUILD_DIR)

.PHONY: all
