/*============================================================================*/
/*
 * AD56XXDefs.h
 *
 *  Created on: Oct 24, 2022
 *      Author: sowarzan
 *
 * AD5623RBRMZ-3 dual DAC 12bit Driver definitions
 *
 */
/*============================================================================*/

#ifndef AD56XXDEFS_HPP_
#define AD56XXDEFS_HPP_
#include <stdint.h>
/**
 * Based on the AD56XX datasheet - page 23:
 * https://www.analog.com/media/en/technical-documentation/data-sheets/ad5623r_43r_63r.pdf
 */
enum AD56XXRegAddrs: uint8_t
{
    AE8_RESET_ADDR      = 0xE8, // Software Reset Command
    AE0_POWER_DOWN_ADDR = 0xE0, // Power Up/Down Function
    AF0_LDAC_ADDR       = 0xF0, // LDAC Setup Command - use nLDAC pin to update DAC register
    AF8_REF_ADDR        = 0xF8, // Reference Setup Function
    ADF_UPDATE_ADDR     = 0xDF, // Write to and update DAC Channel AB
}; // AD56XXRegAddrs

enum AD56XXRegBits: uint8_t
{
    AE8_RESET_BIT              = 0,

    AE0_DACA_BIT               = 0,
    AE0_DACB_BIT               = 1,
    AE0_POWER_DOWN0_BIT        = 4,
    AE0_POWER_DOWN1_BIT        = 5,

    AF0_DACA_BIT               = 0,
    AF0_DACB_BIT               = 1,

    AF8_REF_BIT                = 0,
}; // AD56XXRegBits

enum AD56XXRegBitMasks: uint16_t
{
    AD_CLEAR_REG_MASK    = 0x0000,
    AD_FULL_REG_MASK     = 0xFFFF,

    AE8_RESET_MASK       = (1 << AE8_RESET_BIT),

    AE0_PWRON_MASK       = (1 << AE0_DACA_BIT) |
                           (1 << AE0_DACB_BIT),

    AE0_PWROFF_MASK      = (1 << AE0_POWER_DOWN0_BIT) |
                           (1 << AE0_POWER_DOWN1_BIT) |
                           (1 << AE0_DACA_BIT) |
                           (1 << AE0_DACB_BIT),

    AF0_LDAC_MASK        = (1 << AF0_DACA_BIT) |
                           (1 << AF0_DACB_BIT),

    AF8_REF_MASK         = (1 << AF8_REF_BIT),

}; // AD56XXRegBitMasks

#endif /* AD56XXDEFS_HPP_ */
