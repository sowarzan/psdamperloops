/*============================================================================*/
/*
 * AD56XXDriver.cpp
 *
 *  Created on: Oct 24, 2022
 *      Author: owarzan
 */
/*============================================================================*/

#include "AD56XXDriver.hpp"


/*============================================================================*/
/* AD56XX constants & configs */
/*============================================================================*/
static const std::unordered_map<uint32_t, std::string> AD56XX_REGS_AS_STRING = {
        {AD56XXRegAddrs::AE8_RESET_ADDR,       "AE8_RESET"},
        {AD56XXRegAddrs::AE0_POWER_DOWN_ADDR,  "AE0_POWER_DOWN"},
        {AD56XXRegAddrs::AF0_LDAC_ADDR,        "AF0_LDAC"},
        {AD56XXRegAddrs::AF8_REF_ADDR,         "AF8_REF"},
        {AD56XXRegAddrs::ADF_UPDATE_ADDR,      "ADF_UPDATE"}
};

/* Maximum possible char's length of an one transfer is 128 bits. */
static constexpr uint8_t SPI_CHARLEN_128b_CODE      = 0x00;
static constexpr uint8_t SPI_CHARLEN_MAX            = 0x80; // 128bit

/**
 * SPI input word format described in the AD56XX datasheet
 * Page 22, chapter "INPUT SHIFT REGISTER"
 * https://www.analog.com/media/en/technical-documentation/data-sheets/ad5623r_43r_63r.pdf
 */

static constexpr uint16_t SPI_WORD_ADDR_MASK        = 0xFF;       // A[7:0]
static constexpr uint16_t SPI_WORD_ADDR_BITOFFSET   = 16;
static constexpr uint32_t SPI_WORD_DATA_MASK        = 0x00FFFF;   // D[16:4]
static constexpr uint8_t  SPI_WORD_DATA_BITOFFSET   = 4;

/*============================================================================*/
/* AD56XX Ctors */
/*============================================================================*/
AD56XXDriver::AD56XXDriver(rf_dac_ctrl_driver* const pdrv,
                            const uint32_t inputClkFreq,
                            const uint32_t spiClkFreq,
                            const uint8_t charBitLength):
                pdrv(pdrv),
                inputClkFreq(inputClkFreq),
                spiClkFreq(spiClkFreq),
                charBitLength(charBitLength)
{
    spiInit();
};

/*============================================================================*/
/* AD56XX SPI handlers */
/*============================================================================*/
void AD56XXDriver::spiWaitForTransfer(const std::string extraErrMsg)
{
    uint32_t timeout = 0xFFFF;
    while (isSPITransferPending() and --timeout) {};

    if (not timeout)
    {
        printf("ERROR: AD56XX: writing SPI data failed\n");//) % extraErrMsg;
        throw;
    }
}

const uint8_t AD56XXDriver::spiTxTransfer(const uint32_t addr, const uint32_t data)
{
    uint32_t address    = static_cast<uint32_t>( (addr & SPI_WORD_ADDR_MASK) << SPI_WORD_ADDR_BITOFFSET );
    uint32_t word       = static_cast<uint32_t>( address | data & SPI_WORD_DATA_MASK);

    // TODO: currently only Tx0 in use, other lines can be used in future
    pdrv->dac_spi_master.transmitReceive0.set(word);

    spiWaitForTransfer("transfer is pending\n");
    printf("transfer is pending\n");
    spiStartTransfer();
    spiWaitForTransfer("bus not responding after start transfer");

    // TODO: add mask for work (data only)
    printf("AD56XX: written word Reg '%s' = 0x%x\n", AD56XX_REGS_AS_STRING.at(addr).c_str(), word);
    return 0;
}

void AD56XXDriver::spiSetClkDivider(void)
{
    /**
     *                (    Input Clock Freq     )
     * divider = floor( -------------------  - 1)
     *                (    2 * SPI Clock Freq   )
     */
    uint16_t div = static_cast<uint16_t>((inputClkFreq / 2 / spiClkFreq) - 1);
    pdrv->dac_spi_master.divider.set(div);
    printf("AD56XX: input clk = %lu, SPI desired clock = %lu, clock divider = %lu\n", inputClkFreq, spiClkFreq, div);
}

#include <exception>

void AD56XXDriver::spiInit(void)
{
    printf("AD56XX: initialization DAC SPI in progress...\n");

    spiSetClkDivider();
    spiAutoSlaveSelectEnable(true);
    spiChipSelectEnable(true);
    enableInterrupt(false);
    setEndianess(SPIEndiannes::LSB);
    setMOSILatchEdge(SPILatchEdge::FALLING);
    setMISOLatchEdge(SPILatchEdge::RISING);

    /**
     * "charLength" = [1bit..127bits] or "SPI_CHARLEN_128b_CODE" for 128bits
     **/
    pdrv->dac_spi_master.controlStatus.charLen.set(charBitLength == SPI_CHARLEN_MAX ? SPI_CHARLEN_128b_CODE : charBitLength);

    printf("AD56XX: initialization DAC SPI in progress -> done.\n");
}

/*============================================================================*/
/* AD56XX Initialization handlers */
/*============================================================================*/
void AD56XXDriver::init()
{

    printf("AD56XX: initialization DAC in progress...\n");

    /* Selected Two's Complement Data format */
    spiWriteWord(AD56XXRegAddrs::AE8_RESET_ADDR,      AD56XXRegBitMasks::AE8_RESET_MASK);
    spiWriteWord(AD56XXRegAddrs::AE0_POWER_DOWN_ADDR, AD56XXRegBitMasks::AE0_PWRON_MASK);
    spiWriteWord(AD56XXRegAddrs::AF0_LDAC_ADDR,       AD56XXRegBitMasks::AF0_LDAC_MASK);
    spiWriteWord(AD56XXRegAddrs::AF8_REF_ADDR,        AD56XXRegBitMasks::AF8_REF_MASK);
    spiWriteWord(AD56XXRegAddrs::ADF_UPDATE_ADDR,     AD56XXRegBitMasks::AD_CLEAR_REG_MASK);


    printf("AD56XX: initialization DAC in progress -> done.\n");
}

