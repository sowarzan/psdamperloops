/*============================================================================*/
/**
 * AD5623RBRMZ-3 dual DAC 12bit Driver
 *
 * Based on the AD56XX datasheet:
 * https://www.analog.com/media/en/technical-documentation/data-sheets/ad5623r_43r_63r.pdf
 */
/*============================================================================*/

#ifndef AD56XXDRIVER_HPP_
#define AD56XXDRIVER_HPP_
#include "AD56XXDefs.hpp"
#include <rf-dac_ctrl-lib/rf_dac_ctrl_driver.hpp>


class AD56XXDriver
{
public:
    enum class SPIEndiannes
    {
        LSB,
        MSB
    };

    enum class SPILatchEdge
    {
        FALLING,
        RISING
    };

    enum class SPIRxTxMode
    {
        READ,
        WRITE
    };

private:
    rf_dac_ctrl_driver* const pdrv;
    const uint32_t inputClkFreq;
    const uint32_t spiClkFreq;
    const uint8_t charBitLength;

    /* ===== SPI handlers ===== */
    void spiInit(void);
    void spiStartTransfer(void) const { pdrv->dac_spi_master.controlStatus.goBsy.set(true); };

    const uint8_t spiTxTransfer(const uint32_t addr, const uint32_t data = 0);
    void spiWriteWord(const uint32_t addr, const uint32_t data) { spiTxTransfer(addr, data); };

    void spiSetClkDivider(void);
    void spiChipSelectEnable(const bool enable) { pdrv->dac_spi_master.ss.set(enable); };
    void spiAutoSlaveSelectEnable(const bool enable) { pdrv->dac_spi_master.controlStatus.ass.set(enable); };

    const bool isSPITransferPending(void) const { return pdrv->dac_spi_master.controlStatus.goBsy.get(); };
    void spiWaitForTransfer(const std::string extraErrMsg = "");

    void enableInterrupt(const bool enable) { pdrv->dac_spi_master.controlStatus.ie.set(enable); };
    void setEndianess(const SPIEndiannes mode) { pdrv->dac_spi_master.controlStatus.lsb.set(mode == SPIEndiannes::MSB); };
    void setMOSILatchEdge(const SPILatchEdge mode) { pdrv->dac_spi_master.controlStatus.txNeg.set(mode == SPILatchEdge::FALLING);};
    void setMISOLatchEdge(const SPILatchEdge mode) { pdrv->dac_spi_master.controlStatus.rxNeg.set(mode == SPILatchEdge::FALLING);};

    void reset(void);

public:
    /* No need for checking pdrv nullness - driver wrappers "instance()" never returns NULL
     *
     * "inputClkFreq" - default input clock is the wishbone 50MHz clock.
     **/
    explicit AD56XXDriver(rf_dac_ctrl_driver* const pdrv,
                            const uint32_t inputClkFreq = 50000000,
                            const uint32_t spiClkFreq   = 3125000, // 12.5MHz
                            const uint8_t charBitLength = 24);

    AD56XXDriver()                                 = delete;
    AD56XXDriver(const AD56XXDriver& drv)         = delete;
    AD56XXDriver(AD56XXDriver&& drv)              = delete;

    AD56XXDriver& operator=(AD56XXDriver& drv)    = delete;
    AD56XXDriver& operator=(AD56XXDriver&& drv)   = delete;

    /**
     * Test mode allows to force the ADC outputs (D[12:0] lines) to know test pattern value
     **/
    void init();

    ~AD56XXDriver() = default;
}; // class AD56XXDriver


#endif /* SRC_ALLAFCZ_COMMON_AD56XXDRIVER_H_ */
