/*============================================================================*/
/*
 * AD9512.h
 *
 *  Created on: Nov 09, 2022
 *      Author: sowarzan
 *
 * AD9512 1.2 GHz Clock Distribution IC, 1.6 GHz Inputs, Dividers, Delay Adjust, Five Outputs
 *
 */
/*============================================================================*/

#ifndef AD9512DEFS_HPP_
#define AD9512DEFS_HPP_
#include <stdint.h>
/**
 * Based on the AD9512 datasheet - page 37:
 * https://www.analog.com/media/en/technical-documentation/data-sheets/ad9512.pdf
 */
enum AD9512RegAddrs: uint16_t
{
  A00_SPI_CONF_ADDR           = 0x0000,  // Serial Control Port Configuration
  A34_DELAY_BYPASS4_ADDR      = 0x0034,  // Bypass Delay
  A35_DELAY_FULL_SCALE4_ADDR  = 0x0035,  // Max. Delay Full-Scale
  A36_DELAY_FINE_ADJUST4_ADDR = 0x0036,  // Min. Delay Value
  A3D_LVPECL_OUT0_ADDR        = 0x003D,
  A3E_LVPECL_OUT1_ADDR        = 0x003E,
  A3F_LVPECL_OUT2_ADDR        = 0x003F,
  A40_LVDS_CMOSOUT3_ADDR      = 0x0040,
  A41_LVDS_CMOSOUT4_ADDR      = 0x0041,
  A45_CLOCKS_SELECT_ADDR      = 0x0045, // All Clocks ON, Select CLK1
  A4A_DIVIDER0_ADDR           = 0x004A, // Divide by 2
  A4B_DIVIDER0_ADDR           = 0x004B, // Phase = 0
  A4C_DIVIDER1_ADDR           = 0x004C, // Divide by 4
  A4D_DIVIDER1_ADDR           = 0x004D, // Phase = 0
  A4E_DIVIDER2_ADDR           = 0x004E, // Divide by 8
  A4F_DIVIDER2_ADDR           = 0x004F, // Phase = 0
  A50_DIVIDER3_ADDR           = 0x0050, // Divide by 2
  A51_DIVIDER3_ADDR           = 0x0051, // Phase = 0
  A52_DIVIDER4_ADDR           = 0x0052, // Divide by 4
  A53_DIVIDER4_ADDR           = 0x0053, // Phase = 0
  A58_FUNCTION_ADDR           = 0x0058, // FUNCTION Pin = RESETB
  A5A_UPDATE_REGISTERS_ADDR   = 0x005A // SelfClearing Bit

}; // AD9512RegAddrs

enum AD9512RegBits: uint8_t
{
    A00_SDO_OFF               = 7,
    A00_LONG_INSTRUC          = 4,
    A3D_A3F_POWER_DOWN3_BIT   = 3,
    A3D_A3F_POWER_DOWN2_BIT   = 2,
    A3D_A3F_OUTPUT_LEVEL1_BIT = 1,
    A3D_A3F_OUTPUT_LEVEL0_BIT = 0,
    A45_SELECT_CLK_IN_BIT     = 1,
    A4B_A53_BYPASS_BIT        = 7

}; // AD9512RegBits

enum AD9512RegBitMasks: uint8_t
{
    AD9_CLEAR_REG_MASK = 0x00,
    A00_SPI_MASK    = (1 << A00_SDO_OFF) |
                      (1 << A00_LONG_INSTRUC),
    A3_LVPECL_MASK  = (1 << A3D_A3F_POWER_DOWN3_BIT) |
                      (1 << A3D_A3F_OUTPUT_LEVEL1_BIT),
    A4F_BYPASS_MASK = (1 << A4B_A53_BYPASS_BIT),

}; // AD9512RegBitMasks

#endif /* AD9512DEFS_HPP_ */
