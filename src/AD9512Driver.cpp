/*============================================================================*/
/*
 * AD9512Driver.cpp
 *
 *  Created on: Nov 09, 2022
 *      Author: sowarzan
 */
/*============================================================================*/

#include "AD9512Driver.hpp"


/*============================================================================*/
/* AD9512 constants & configs */
/*============================================================================*/
static const std::unordered_map<uint32_t, std::string> AD9512_REGS_AS_STRING = {
        {AD9512RegAddrs::A00_SPI_CONF_ADDR,           "A00_SCP_CONF"},
        {AD9512RegAddrs::A34_DELAY_BYPASS4_ADDR,      "A34_DELAY_BYPASS4"},
        {AD9512RegAddrs::A35_DELAY_FULL_SCALE4_ADDR,  "A35_DELAY_FULL_SCALE"},
        {AD9512RegAddrs::A36_DELAY_FINE_ADJUST4_ADDR, "A36_DELAY_FINE_ADJUS"},
        {AD9512RegAddrs::A3D_LVPECL_OUT0_ADDR,        "A3D_LVPECL_OUT0"},
        {AD9512RegAddrs::A3E_LVPECL_OUT1_ADDR,        "A3E_LVPECL_OUT1"},
        {AD9512RegAddrs::A3F_LVPECL_OUT2_ADDR,        "A3F_LVPECL_OUT2"},
        {AD9512RegAddrs::A40_LVDS_CMOSOUT3_ADDR,      "A40_LVDS_CMOSOUT3"},
        {AD9512RegAddrs::A41_LVDS_CMOSOUT4_ADDR,      "A41_LVDS_CMOSOUT4"},
        {AD9512RegAddrs::A45_CLOCKS_SELECT_ADDR,      "A45_CLOCKS_SELECT"},
        {AD9512RegAddrs::A4A_DIVIDER0_ADDR,           "A4A_DIVIDER0"},
        {AD9512RegAddrs::A4B_DIVIDER0_ADDR,           "A4B_DIVIDER0"},
        {AD9512RegAddrs::A4C_DIVIDER1_ADDR,           "A4C_DIVIDER1"},
        {AD9512RegAddrs::A4D_DIVIDER1_ADDR,           "A4D_DIVIDER1"},
        {AD9512RegAddrs::A4E_DIVIDER2_ADDR,           "A4E_DIVIDER2"},
        {AD9512RegAddrs::A4F_DIVIDER2_ADDR,           "A4F_DIVIDER2"},
        {AD9512RegAddrs::A50_DIVIDER3_ADDR,           "A50_DIVIDER3"},
        {AD9512RegAddrs::A51_DIVIDER3_ADDR,           "A51_DIVIDER3"},
        {AD9512RegAddrs::A52_DIVIDER4_ADDR,           "A52_DIVIDER4"},
        {AD9512RegAddrs::A53_DIVIDER4_ADDR,           "A53_DIVIDER4"},
        {AD9512RegAddrs::A58_FUNCTION_ADDR,           "A58_FUNCTION"},
        {AD9512RegAddrs::A5A_UPDATE_REGISTERS_ADDR,   "A5A_UPDATE_REGISTERS"}
};

/* Maximum possible char's length of an one transfer is 128 bits. */
static constexpr uint8_t SPI_CHARLEN_128b_CODE      = 0x00;
static constexpr uint8_t SPI_CHARLEN_MAX            = 0x80; // 128bit

/**
 * SPI input word format described in the AD9512 datasheet
 * Page 37, chapter "REGISTER MAP AND DESCRIPTION"
 * https://www.analog.com/media/en/technical-documentation/data-sheets/ad9512.pdf
 */
static constexpr uint16_t SPI_WORD_RW_BIT           = 15;
static constexpr uint32_t SPI_WORD_READ_MODE_MASK   = (1 << SPI_WORD_RW_BIT);

static constexpr uint16_t SPI_WORD_ADDR_MASK        = 0x7F;     // A[6:0]
static constexpr uint16_t SPI_WORD_ADDR_BITOFFSET   = 8;
static constexpr uint32_t SPI_WORD_DATA_MASK        = 0xFF;     // D[7:0]


/*============================================================================*/
/* AD9512 Ctors */
/*============================================================================*/
AD9512Driver::AD9512Driver(rf_clks_ctrl_driver* const pdrv,
                            const uint32_t inputClkFreq,
                            const uint32_t spiClkFreq,
                            const uint8_t charBitLength):
                pdrv(pdrv),
                inputClkFreq(inputClkFreq),
                spiClkFreq(spiClkFreq),
                charBitLength(charBitLength)
{
    spiInit();
};

/*============================================================================*/
/* AD9512 SPI handlers */
/*============================================================================*/
void AD9512Driver::spiWaitForTransfer(const std::string extraErrMsg)
{
    uint32_t timeout = 0xFFFF;
    while (isSPITransferPending() and --timeout) {};

    if (not timeout)
    {
        printf("ERROR: AD9512: writing SPI data failed\n");//) % extraErrMsg;
        throw;
        //LOG_ERROR_IF(logger, msg.str().c_str());
    }
}

const uint8_t AD9512Driver::spiRxTxTransfer(const SPIRxTxMode mode, const uint32_t addr, const uint32_t data)
{
    uint32_t address    = static_cast<uint32_t>( (addr & SPI_WORD_ADDR_MASK) << SPI_WORD_ADDR_BITOFFSET );
    uint32_t word       = static_cast<uint32_t>( address | (data & SPI_WORD_DATA_MASK) );
    uint16_t readAddr;
    uint8_t rxData;
    if (mode == SPIRxTxMode::READ)
    {
        readAddr = static_cast<uint16_t>(SPI_WORD_READ_MODE_MASK | addr);
        spiAutoSlaveSelectEnable(false);
        pdrv->clk_spi_master.controlStatus.charLen.set(16);
        pdrv->clk_spi_master.transmitReceive0.set(readAddr);
        pdrv->clk_spi_master.ss.set(1);
        spiWaitForTransfer("adress transfer is pending\n");
        spiStartTransfer();
        spiWaitForTransfer("bus not responding after start transfer");
        pdrv->clk_spi_master.transmitReceive0.set(0);
        pdrv->clk_spi_master.controlStatus.charLen.set(8);
        pdrv->clk_spi_master.controlStatus.dir.set(1);
        spiWaitForTransfer("reading is pending\n");
        spiStartTransfer();
        spiWaitForTransfer("bus not responding after start reading");
        rxData = static_cast<uint8_t>(pdrv->clk_spi_master.transmitReceive0.get());
        pdrv->clk_spi_master.ss.set(0);
        pdrv->clk_spi_master.controlStatus.dir.set(0);
        printf("AD9512: read word Reg '%s' = 0x%x\n", AD9512_REGS_AS_STRING.at(addr).c_str(), rxData);
        return rxData;
    }
    else if (mode == SPIRxTxMode::WRITE)
    {
        // TODO: currently only Tx0 in use, other lines can be used in future
        pdrv->clk_spi_master.transmitReceive0.set(word);
        spiWaitForTransfer("transfer is pending\n");
        printf("transfer is pending\n");
        spiStartTransfer();
        spiWaitForTransfer("bus not responding after start transfer");
        // TODO: add mask for work (data only)
        printf("AD9512: written word Reg '%s' = 0x%x\n", AD9512_REGS_AS_STRING.at(addr).c_str(), word);
        return 0;
    }

}

void AD9512Driver::spiSetClkDivider(void)
{
    /**
     *                (    Input Clock Freq     )
     * divider = floor( -------------------  - 1)
     *                (    2 * SPI Clock Freq   )
     */
    uint16_t div = static_cast<uint16_t>((inputClkFreq / 2 / spiClkFreq) - 1);
    pdrv->clk_spi_master.divider.set(div);
    printf("AD9512: input clk = %lu, SPI desired clock = %lu, clock divider = %lu\n", inputClkFreq, spiClkFreq, div);
}

#include <exception>

void AD9512Driver::spiInit(void)
{
    printf("AD9512: initialization clks SPI in progress...\n");

    spiSetClkDivider();
    pdrv->clk_spi_master.controlStatus.dir.set(0);
    spiAutoSlaveSelectEnable(true);
    spiChipSelectEnable(true);
    enableInterrupt(false);
    setEndianess(SPIEndiannes::LSB);
    setMOSILatchEdge(SPILatchEdge::FALLING);
    setMISOLatchEdge(SPILatchEdge::RISING);

    /**
     * "charLength" = [1bit..127bits] or "SPI_CHARLEN_128b_CODE" for 128bits
     **/
    pdrv->clk_spi_master.controlStatus.charLen.set(charBitLength == SPI_CHARLEN_MAX ? SPI_CHARLEN_128b_CODE : charBitLength);

    spiWriteWord(AD9512RegAddrs::A00_SPI_CONF_ADDR,      AD9512RegBitMasks::A00_SPI_MASK);

    printf("AD9512: initialization clks SPI in progress -> done.\n");
}

/*============================================================================*/
/* AD9512 Initialization handlers */
/*============================================================================*/
void AD9512Driver::init()
{

    printf("AD9512: initialization clks in progress...\n");
    spiWriteWord(AD9512RegAddrs::A4A_DIVIDER0_ADDR,    AD9512RegBitMasks::AD9_CLEAR_REG_MASK);
    spiWriteWord(AD9512RegAddrs::A5A_UPDATE_REGISTERS_ADDR,      0x01);
    spiWriteWord(AD9512RegAddrs::A4B_DIVIDER0_ADDR,    AD9512RegBitMasks::A4F_BYPASS_MASK);
    spiWriteWord(AD9512RegAddrs::A5A_UPDATE_REGISTERS_ADDR,      0x01);
    spiWriteWord(AD9512RegAddrs::A3D_LVPECL_OUT0_ADDR,   0x08);
    spiWriteWord(AD9512RegAddrs::A5A_UPDATE_REGISTERS_ADDR,      0x01);
    spiWriteWord(AD9512RegAddrs::A4D_DIVIDER1_ADDR,      AD9512RegBitMasks::A4F_BYPASS_MASK);
    spiWriteWord(AD9512RegAddrs::A5A_UPDATE_REGISTERS_ADDR,      0x01);
    spiWriteWord(AD9512RegAddrs::A3E_LVPECL_OUT1_ADDR,   AD9512RegBitMasks::A3_LVPECL_MASK);
    spiWriteWord(AD9512RegAddrs::A5A_UPDATE_REGISTERS_ADDR,      0x01);
    spiWriteWord(AD9512RegAddrs::A4F_DIVIDER2_ADDR,      AD9512RegBitMasks::A4F_BYPASS_MASK);
    spiWriteWord(AD9512RegAddrs::A5A_UPDATE_REGISTERS_ADDR,      0x01);
    spiWriteWord(AD9512RegAddrs::A3F_LVPECL_OUT2_ADDR,   AD9512RegBitMasks::A3_LVPECL_MASK);
    spiWriteWord(AD9512RegAddrs::A5A_UPDATE_REGISTERS_ADDR,      0x01);

    printf("AD9512: initialization clks in progress -> done.\n");
}
