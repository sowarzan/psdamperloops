/*============================================================================*/
/**
 * AD9512 1.2 GHz Clock Distribution Driver
 *
 * Based on the AD9512 datasheet:
 * https://www.analog.com/media/en/technical-documentation/data-sheets/ad9512.pdf
 */
/*============================================================================*/

#ifndef AD9512DRIVER_HPP_
#define AD9512DRIVER_HPP_
#include "AD9512Defs.hpp"
#include <rf-clks_ctrl-lib/rf_clks_ctrl_driver.hpp>

class AD9512Driver
{
public:
    enum class SPIEndiannes
    {
        LSB,
        MSB
    };

    enum class SPILatchEdge
    {
        FALLING,
        RISING
    };

    enum class SPIRxTxMode
    {
        READ,
        WRITE
    };

private:
    rf_clks_ctrl_driver* const pdrv;
    const uint32_t inputClkFreq;
    const uint32_t spiClkFreq;
    const uint8_t charBitLength;

    /* ===== SPI handlers ===== */
    void spiInit(void);
    void spiStartTransfer(void) const { pdrv->clk_spi_master.controlStatus.goBsy.set(true); };

    const uint8_t spiRxTxTransfer(const SPIRxTxMode mode, const uint32_t addr, const uint32_t data = 0);
    void spiWriteWord(const uint32_t addr, const uint32_t data) { spiRxTxTransfer(SPIRxTxMode::WRITE, addr, data); };
    const uint8_t spiReadWord(const uint32_t addr) { return spiRxTxTransfer(SPIRxTxMode::READ, addr); };

    void spiSetClkDivider(void);
    void spiChipSelectEnable(const bool enable) { pdrv->clk_spi_master.ss.set(enable); };
    void spiAutoSlaveSelectEnable(const bool enable) { pdrv->clk_spi_master.controlStatus.ass.set(enable); };

    const bool isSPITransferPending(void) const { return pdrv->clk_spi_master.controlStatus.goBsy.get(); };
    void spiWaitForTransfer(const std::string extraErrMsg = "");

    void enableInterrupt(const bool enable) { pdrv->clk_spi_master.controlStatus.ie.set(enable); };
    void setEndianess(const SPIEndiannes mode) { pdrv->clk_spi_master.controlStatus.lsb.set(mode == SPIEndiannes::MSB); };
    void setMOSILatchEdge(const SPILatchEdge mode) { pdrv->clk_spi_master.controlStatus.txNeg.set(mode == SPILatchEdge::FALLING);};
    void setMISOLatchEdge(const SPILatchEdge mode) { pdrv->clk_spi_master.controlStatus.rxNeg.set(mode == SPILatchEdge::FALLING);};

    void reset(void);

public:
    /* No need for checking pdrv nullness - driver wrappers "instance()" never returns NULL
     *
     * "inputClkFreq" - default input clock is the wishbone 50MHz clock.
     **/
    explicit AD9512Driver(rf_clks_ctrl_driver* const pdrv,
                            const uint32_t inputClkFreq = 50000000,
                            const uint32_t spiClkFreq   = 3125000, // 12.5MHz
                            const uint8_t charBitLength = 24);

    AD9512Driver()                                = delete;
    AD9512Driver(const AD9512Driver& drv)         = delete;
    AD9512Driver(AD9512Driver&& drv)              = delete;

    AD9512Driver& operator=(AD9512Driver& drv)    = delete;
    AD9512Driver& operator=(AD9512Driver&& drv)   = delete;

    void init();

    ~AD9512Driver() = default;
}; // class AD9512Driver


#endif /* AD9512DRIVER_HPP_ */
