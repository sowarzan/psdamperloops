 /*============================================================================*/
/*
 * ADCDriver.cpp
 *
 *  Created on: Oct 13, 2022
 *      Author: sowarzan
 */
 /*============================================================================*/

#include "ADCDriver.hpp"
#include <unistd.h>

static unsigned int microsecond = 1000000;

//static constexpr char LOGFORMAT_HEX[] = "%-45s = 0x%llx";
static constexpr char LOGFORMAT_INT[] = "%-45s = %ld";
static constexpr char LOGFORMAT_STR[] = "%-45s = %s";

constexpr char ADCDriver::STATUS_FILE_PREFIX[];

static constexpr uint32_t SERDES_SYNC_TIMEOUT     = 20;   // in seconds
static constexpr uint32_t SERDES_CHANNEL_BITMASK  = 0xF;  // 4 channels [0..3]

/*============================================================================*/
/* FMC ADC subsamp 125MSps general methods */
/*============================================================================*/
void ADCDriver::setSerDesInputDelays(const std::array<uint32_t, ADC_CHANNEL_NUMBER> outaDelay)
{
    pdrv->outaIdelayIn_0.set(outaDelay[0]);
    pdrv->outaIdelayIn_1.set(outaDelay[1]);
    pdrv->outaIdelayIn_2.set(outaDelay[2]);
    pdrv->outaIdelayIn_3.set(outaDelay[3]);

    /* Reload OUTA delays - autoclear */
    pdrv->control.outaIdelayLoad.set(SERDES_CHANNEL_BITMASK);

    printf("ADC125: adc0 input delays: OUTA = %u\n", outaDelay[0]);
    printf("ADC125: adc1 input delays: OUTA = %u\n", outaDelay[1]);
    printf("ADC125: adc2 input delays: OUTA = %u\n", outaDelay[2]);
    printf("ADC125: adc3 input delays: OUTA = %u\n", outaDelay[3]);
}

const std::array<uint32_t, ADC_CHANNEL_NUMBER> ADCDriver::getSerDesInputDelays() const
{
    uint32_t delay0 = pdrv->outaIdelayIn_0.get();
    uint32_t delay1 = pdrv->outaIdelayIn_1.get();
    uint32_t delay2 = pdrv->outaIdelayIn_2.get();
    uint32_t delay3 = pdrv->outaIdelayIn_3.get();

    return std::array<uint32_t, ADC_CHANNEL_NUMBER>{delay0, delay1, delay2, delay3};
}

void ADCDriver::resetIdelays(void)
{
    pdrv->control.asyncRstEnable.set(true);
    pdrv->control.asyncRstEnable.set(false);
}


/*============================================================================*/
/* FMC ADC subsamp 125MSps initialization */
/*============================================================================*/
void ADCDriver::init(ADCConfig cfg)
{
    printf("ADC125: initialization in progress...\n");

    printf("ADC125: initialization ADC chip (LTC218X) in progress...\n");
    adcDrv.init(cfg.testsEnabled);

    setADCInputClkSrc(cfg.clkSrc);


    printf("ADC125: resetting IDelays, SerDes, bitslip logic, error status latches and Fs clock in progress...\n");
    resetIdelays();
    reset();


    printf("ADC125: setting SerDes OUTA channels input delays in progress...\n");
    setSerDesInputDelays(cfg.outaInputDelays);

    auto outaInputDelays = getSerDesInputDelays();

    printf("ADC125: readback adc0 input delays: OUTA = %u\n", outaInputDelays[0]);
    printf("ADC125: readback adc1 input delays: OUTA = %u\n", outaInputDelays[1]);
    printf("ADC125: readback adc2 input delays: OUTA = %u\n", outaInputDelays[2]);
    printf("ADC125: readback adc3 input delays: OUTA = %u\n", outaInputDelays[3]);

    printf("ADC125: control readback = '0x%x'\n", pdrv->control.get());

    printf("ADC125: adc0 readback = '0x%x'\n", pdrv->adc0.get());
    printf("ADC125: adc1 readback = '0x%x'\n", pdrv->adc1.get());
    printf("ADC125: adc2 readback = '0x%x'\n", pdrv->adc2.get());
    printf("ADC125: adc3 readback = '0x%x'\n", pdrv->adc3.get());

   printf("ADC125: initialization in progress -> done.\n");
}


/*============================================================================*/
/* FMC ADC subsamp 125MSps patternTest */
/*============================================================================*/
void ADCDriver::patternTest(ADCConfig cfg)
{
    printf("ADC125: Testing in progress...\n");

    printf("ADC125: setting SerDes OUTA channels input delays in progress...\n");
    for ( unsigned int i = 0; i <= 31; i++ )
    {
        cfg.outaInputDelays = {i, 0, 0, 0};
        setSerDesInputDelays(cfg.outaInputDelays);

        auto outaInputDelays = getSerDesInputDelays();

        printf("ADC125: readback adc0 input delays: OUTA = %u\n", outaInputDelays[0]);
        printf("ADC125: readback adc1 input delays: OUTA = %u\n", outaInputDelays[1]);
        printf("ADC125: readback adc2 input delays: OUTA = %u\n", outaInputDelays[2]);
        printf("ADC125: readback adc3 input delays: OUTA = %u\n", outaInputDelays[3]);
        usleep(2 * microsecond);
    }



   printf("ADC125: test in progress -> done.\n");
}