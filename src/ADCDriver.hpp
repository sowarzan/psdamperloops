/*============================================================================*/
/*
 * ADCDriver.h
 *
 *  Created on: Oct 3, 2022
 *      Author: sowarzan
 *
 * ADC 125MSps 4cha 16b Driver
 *
/*============================================================================*/

#ifndef ADCDRIVER_HPP_
#define ADCDRIVER_HPP_

#include <rf-adc_ctrl-lib/rf_adc_ctrl_driver.hpp>
#include <stdint.h>
#include "LTC218XDriver.hpp"

static constexpr const int ADC_CHANNEL_NUMBER = 4;

// Clock selector
// CLK_SEL<0>     |        CLK_SEL<1>
// 0:CLK0=FPCLK0  |  0:ADC0=CLK0  1:ADC0=CLK0
// 1:CLK0=BPCLK1  |    ADC1=CLK0    ADC1=FPCLK1
// |    DAC0=CLK0    DAC0=FPCLK2
// |    DAC1=CLK0    DAC1=FPCLK3
enum ADCClkSrcEnum
{
    ADC_CLK_SRC_FPCLK_0_ONLY  = 0,
    ADC_CLK_SRC_FPCLK_ALL,
    ADC_CLK_SRC_BPCLK_0_ONLY,
    ADC_CLK_SRC_BPCLK_ALL,
};
/*============================================================================*/
/* ADC 125MSps 4cha 16b Driver */
/*============================================================================*/
typedef struct ADCConfig
{
     ADCClkSrcEnum clkSrc;

     std::array<uint32_t, ADC_CHANNEL_NUMBER> outaInputDelays;

    /* Enabled all possible tests for ADCDriver (E.g. pattern tests for LTC217X)*/
     bool testsEnabled;
} ADCConfig;

class ADCDriver
{
public:
    static constexpr char       STATUS_FILE_PREFIX[]    = "/tmp/fmcadc125m14b_initialized_";

    enum class ADCOutputLine
    {
        OUTA,
        OUTB
    };

private:
    rf_adc_ctrl_driver* const pdrv;

    LTC218XDriver   adcDrv;

    void enableSerDes(const bool enable);

    void setSerDesInputDelays(const std::array<uint32_t, ADC_CHANNEL_NUMBER> outaInputDelay);
    const std::array<uint32_t, ADC_CHANNEL_NUMBER> getSerDesInputDelays() const;

    void waitForSerDesSync(void);

    void reset(void) { pdrv->control.rst.set(true); }; // autoclear
    void resetIdelays(void);

    void setADCInputClkSrc(const ADCClkSrcEnum src) { pdrv->control.adcClkmuxSel.set(src); };

public:
    /* No need for checking pdrv nullness - driver wrappers "instance()" never returns NULL */
    explicit ADCDriver(rf_adc_ctrl_driver* const pdrv):
        pdrv(pdrv), adcDrv(pdrv)
        {};

    ADCDriver()                             = delete;
    ADCDriver(const ADCDriver& drv)         = delete;
    ADCDriver(ADCDriver&& drv)              = delete;

    ADCDriver& operator=(ADCDriver& drv)    = delete;
    ADCDriver& operator=(ADCDriver&& drv)   = delete;

    void init(ADCConfig cfg);

    void patternTest(ADCConfig cfg);

    ~ADCDriver() = default;
}; // class ADCDriver



#endif /* ADCDRIVER_HPP_ */
