/*============================================================================*/
/*
 * LTC218XDefs.h
 *
 *  Created on: Oct 11, 2022
 *      Author: sowarzan
 *
 * LTC2185 ADC 125MSps Driver definitions
 *
 */
/*============================================================================*/

#ifndef LTC218XDEFS_HPP_
#define LTC218XDEFS_HPP_
#include <stdint.h>
/**
 * Based on the LTC218X datasheet - page 28:
 * https://www.analog.com/media/en/technical-documentation/data-sheets/218543f.pdf
 */
enum LTC218XRegAddrs: uint8_t
{
    A0_RESET_ADDR = 0x00,
    A1_POWER_DOWN_ADDR,     // Format & Power Control
    A2_TIMING_ADDR,         // ADC output clock configuration
    A3_OUTPUT_ADDR,         // I/O voltage and general configuration
    A4_DATA_FORMAT_ADDR,    // Pattern test and advance data configuration
};

enum LTC218XTestPatters: uint16_t
{
    DISABLED          = 0x0, // Digital Output Test Patterns Off
    OUTPUT_ALL_OFF    = 0x1, // All Digital Outputs = 0
    OUTPUT_ALL_ON     = 0x2, // All Digital Outputs = 1
    OUTPUT_ALTERNATE0 = 0x5, // Alternate Between 5555 and AAAA
    OUTPUT_ALTERNATE1 = 0x7, // Alternate Between 0000 and FFFF
};

enum LTC218XRegBits: uint8_t
{
    A0_RESET_BIT                   = 7,

    A1_POWER_DOWN_PWROFF0_BIT      = 0,
    A1_POWER_DOWN_PWROFF1_BIT      = 1,

    A2_TIMING_DCS_BIT              = 0,
    A2_TIMING_CLKPHASE0_BIT        = 1,
    A2_TIMING_CLKPHASE1_BIT        = 2,
    A2_TIMING_CLKINV_BIT           = 3,

    A3_OUTPUT_OUTMODE0_BIT         = 0,
    A3_OUTPUT_OUTMODE1_BIT         = 1,
    A3_OUTPUT_OUTOFF_BIT           = 2,
    A3_OUTPUT_TERMON_BIT           = 3,
    A3_OUTPUT_ILVDS0_BIT           = 4,
    A3_OUTPUT_ILVDS1_BIT           = 5,
    A3_OUTPUT_ILVDS2_BIT           = 6,

    A4_DATA_FORMAT_TWOSCOMP_BIT    = 0,
    A4_DATA_FORMAT_RAND_BIT        = 1,
    A4_DATA_FORMAT_ABP_BIT         = 2,
    A4_DATA_FORMAT_OUTTEST0_BIT    = 3,
    A4_DATA_FORMAT_OUTTEST1_BIT    = 4,
    A4_DATA_FORMAT_OUTTEST2_BIT    = 5,
};

enum LTC218XRegBitMasks: uint8_t
{
    LTC_CLEAR_REG_MASK      = 0x00,

    A0_RESET_MASK           = (1 << A0_RESET_BIT),

    A1_PWROFF_MASK          = (1 << A1_POWER_DOWN_PWROFF0_BIT) |
                              (1 << A1_POWER_DOWN_PWROFF1_BIT),

    A2_DCS_MASK             = (1 << A2_TIMING_DCS_BIT),
    A2_CLKPHASE_MASK        = (1 << A2_TIMING_CLKPHASE0_BIT) |
                              (1 << A2_TIMING_CLKPHASE1_BIT),
    A2_CLKINV_MASK          = (1 << A2_TIMING_CLKINV_BIT),

    A3_OUTMODE_MASK         = (1 << A3_OUTPUT_OUTMODE1_BIT),
    A3_OUTOFF_MASK          = (1 << A3_OUTPUT_OUTOFF_BIT),
    A3_TERMON_MASK          = (1 << A3_OUTPUT_TERMON_BIT),
    A3_ILVDS_MASK           = (1 << A3_OUTPUT_ILVDS0_BIT) |
                              (1 << A3_OUTPUT_ILVDS1_BIT) |
                              (1 << A3_OUTPUT_ILVDS2_BIT),

    A4_NOUTTEST_MASK        = (1 << A4_DATA_FORMAT_ABP_BIT)  |
                              (1 << A4_DATA_FORMAT_RAND_BIT),

    A4_TWOSCOMP_MASK        = (1 << A4_DATA_FORMAT_TWOSCOMP_BIT),
    A4_RAND_MASK            = (1 << A4_DATA_FORMAT_RAND_BIT),
    A4_ABP_MASK             = (1 << A4_DATA_FORMAT_ABP_BIT),
    A4_OUTTEST_MASK         = (1 << A4_DATA_FORMAT_OUTTEST0_BIT) |
                              (1 << A4_DATA_FORMAT_OUTTEST2_BIT),
};

#endif /* LTC218XDEFS_HPP_ */
