/*============================================================================*/
/*
 * LTC218XDriver.cpp
 *
 *  Created on: Oct 11, 2022
 *      Author: sowarzan
 */
/*============================================================================*/

#include "LTC218XDriver.hpp"


/*============================================================================*/
/* LTC218X constants & configs */
/*============================================================================*/
static const std::unordered_map<uint32_t, std::string> LTC218X_REGS_AS_STRING = {
        {LTC218XRegAddrs::A0_RESET_ADDR,       "A0_RESET"},
        {LTC218XRegAddrs::A1_POWER_DOWN_ADDR,  "A1_POWER_DOWN"},
        {LTC218XRegAddrs::A2_TIMING_ADDR,      "A2_TIMING"},
        {LTC218XRegAddrs::A3_OUTPUT_ADDR,      "A3_OUTPUT"},
        {LTC218XRegAddrs::A4_DATA_FORMAT_ADDR, "A4_DATA_FORMAT"}
};

/* Maximum possible char's length of an one transfer is 128 bits. */
static constexpr uint8_t SPI_CHARLEN_128b_CODE      = 0x00;
static constexpr uint8_t SPI_CHARLEN_MAX            = 0x80; // 128bit

/**
 * SPI input word format described in the LTC218X datasheet
 * Page 27, chapter "Application Information/Serial Programming Mode"
 * https://www.analog.com/media/en/technical-documentation/data-sheets/218543f.pdf
 */
static constexpr uint16_t SPI_WORD_RW_BIT           = 15;
static constexpr uint16_t SPI_WORD_READ_MODE_MASK   = (1 << SPI_WORD_RW_BIT);

static constexpr uint16_t SPI_WORD_ADDR_MASK        = 0x7F;     // A[6:0]
static constexpr uint16_t SPI_WORD_ADDR_BITOFFSET   = 8;
static constexpr uint16_t SPI_WORD_DATA_MASK        = 0x00FF;   // D[7:0]

/* For test mode, output value is always 4 times the input pattern */
static constexpr uint32_t TEST_PATTERN_OUTPUT_POWER = 0x4;
static constexpr uint32_t TEST_PATTERN_MAX          = 0x4000;
static constexpr uint32_t TEST_PATTERN_CHANNEL_MASK = 0xFFFF;

/*============================================================================*/
/* LTC218X Ctors */
/*============================================================================*/
LTC218XDriver::LTC218XDriver(rf_adc_ctrl_driver* const pdrv,
                            const uint32_t inputClkFreq,
                            const uint32_t spiClkFreq,
                            const uint8_t charBitLength):
                pdrv(pdrv),
                inputClkFreq(inputClkFreq),
                spiClkFreq(spiClkFreq),
                charBitLength(charBitLength)
{
    spiInit();
};

/*============================================================================*/
/* LTC218X SPI handlers */
/*============================================================================*/
void LTC218XDriver::spiWaitForTransfer(const std::string extraErrMsg)
{
    uint32_t timeout = 0xFFFF;
    while (isSPITransferPending() and --timeout) {};

    if (not timeout)
    {
        printf("ERROR: LTC218X: writing SPI data failed\n");//) % extraErrMsg;
        throw;
        //LOG_ERROR_IF(logger, msg.str().c_str());
    }
}

const uint8_t LTC218XDriver::spiRxTxTransfer(const SPIRxTxMode mode, const uint32_t addr, const uint32_t data)
{
    uint16_t address    = static_cast<uint16_t>( (addr & SPI_WORD_ADDR_MASK) << SPI_WORD_ADDR_BITOFFSET );
    uint16_t word       = static_cast<uint16_t>( address | (data & SPI_WORD_DATA_MASK) );

    if (mode == SPIRxTxMode::READ)
        word = static_cast<uint16_t>(SPI_WORD_READ_MODE_MASK | address);

    // TODO: currently only Tx0 in use, other lines can be used in future
    pdrv->adc_spi_master.transmitReceive0.set(word);

    spiWaitForTransfer("transfer is pending\n");
    printf("transfer is pending\n");
    spiStartTransfer();
    spiWaitForTransfer("bus not responding after start transfer");

    if (mode == SPIRxTxMode::READ)
    {
        uint8_t rxData = static_cast<uint8_t>(pdrv->adc_spi_master.transmitReceive0.get());
        printf("LTC218X: read word Reg '%s' = 0x%x\n", LTC218X_REGS_AS_STRING.at(addr).c_str(), rxData);
        return rxData;
    }

    // TODO: add mask for work (data only)
    printf("LTC218X: written word Reg '%s' = 0x%x\n", LTC218X_REGS_AS_STRING.at(addr).c_str(), word);
    return 0;
}

void LTC218XDriver::spiSetClkDivider(void)
{
    /**
     *                (    Input Clock Freq     )
     * divider = floor( -------------------  - 1)
     *                (    2 * SPI Clock Freq   )
     */
    uint16_t div = static_cast<uint16_t>((inputClkFreq / 2 / spiClkFreq) - 1);
    pdrv->adc_spi_master.divider.set(div);
    printf("LTC218X: input clk = %lu, SPI desired clock = %lu, clock divider = %lu\n", inputClkFreq, spiClkFreq, div);
}

#include <exception>

void LTC218XDriver::spiInit(void)
{
    printf("LTC218X: initialization ADC SPI in progress...\n");

    spiSetClkDivider();
    spiAutoSlaveSelectEnable(true);
    spiChipSelectEnable(true);
    enableInterrupt(false);
    setEndianess(SPIEndiannes::LSB);
    setMOSILatchEdge(SPILatchEdge::FALLING);
    setMISOLatchEdge(SPILatchEdge::RISING);

    /**
     * "charLength" = [1bit..127bits] or "SPI_CHARLEN_128b_CODE" for 128bits
     **/
    pdrv->adc_spi_master.controlStatus.charLen.set(charBitLength == SPI_CHARLEN_MAX ? SPI_CHARLEN_128b_CODE : charBitLength);

    printf("LTC218X: initialization ADC SPI in progress -> done.\n");
}

void LTC218XDriver::setTestPattern(const LTC218XTestPatters pattern)
{

    uint16_t test = pattern & LTC218XRegBitMasks::A4_OUTTEST_MASK;

    spiWriteWord(LTC218XRegAddrs::A4_DATA_FORMAT_ADDR, test);

    printf("LTC218X: test: written pattern = 0x%x\n", pattern);
}

/*============================================================================*/
/* LTC218X Initialization handlers */
/*============================================================================*/
void LTC218XDriver::init(const bool testModeEnabled, const uint16_t testPattern)
{

    printf("LTC218X: initialization ADC in progress...\n");

    /* Selected Two's Complement Data format */
    spiWriteWord(LTC218XRegAddrs::A0_RESET_ADDR,      LTC218XRegBitMasks::LTC_CLEAR_REG_MASK);
    spiWriteWord(LTC218XRegAddrs::A1_POWER_DOWN_ADDR, LTC218XRegBitMasks::LTC_CLEAR_REG_MASK);
    spiWriteWord(LTC218XRegAddrs::A2_TIMING_ADDR,     LTC218XRegBitMasks::LTC_CLEAR_REG_MASK);
    spiWriteWord(LTC218XRegAddrs::A3_OUTPUT_ADDR,     LTC218XRegBitMasks::A3_OUTMODE_MASK);

    if (testModeEnabled)
    {
        printf("LTC218X: test mode enabled.\n");
        runPatternTests();
        return;
    }
    else
    {
        pdrv->control.outTest.set(0);
        spiWriteWord(LTC218XRegAddrs::A4_DATA_FORMAT_ADDR, LTC218XRegBitMasks::A4_NOUTTEST_MASK);
    }

    printf("LTC218X: initialization ADC in progress -> done.\n");
}

/*============================================================================*/
/* LTC218X Expert Test handlers */
/*============================================================================*/
void LTC218XDriver::runPatternTests()
{
    printf("LTC218X: test: pattern test bench progress...\n");
    pdrv->control.outTest.set(1);
    spiWriteWord(LTC218XRegAddrs::A4_DATA_FORMAT_ADDR, LTC218XRegBitMasks::A4_OUTTEST_MASK);

    printf("LTC218X: test: pattern test bench progress -> done.\n");

}

