/*============================================================================*/
/**
 * LTC2185 ADC 125MSps Driver
 *
 * Based on the LTC218X datasheet:
 * https://www.analog.com/media/en/technical-documentation/data-sheets/218543f.pdf
 */
/*============================================================================*/

#ifndef LTC218XDRIVER_HPP_
#define LTC218XDRIVER_HPP_
#include "LTC218XDefs.hpp"
#include <rf-adc_ctrl-lib/rf_adc_ctrl_driver.hpp>


class LTC218XDriver
{
public:
    enum class SPIEndiannes
    {
        LSB,
        MSB
    };

    enum class SPILatchEdge
    {
        FALLING,
        RISING
    };

    enum class SPIRxTxMode
    {
        READ,
        WRITE
    };

private:
    rf_adc_ctrl_driver* const pdrv;
    const uint32_t inputClkFreq;
    const uint32_t spiClkFreq;
    const uint8_t charBitLength;

    /* ===== SPI handlers ===== */
    void spiInit(void);
    void spiStartTransfer(void) const { pdrv->adc_spi_master.controlStatus.goBsy.set(true); };

    const uint8_t spiRxTxTransfer(const SPIRxTxMode mode, const uint32_t addr, const uint32_t data = 0);
    void spiWriteWord(const uint32_t addr, const uint32_t data) { spiRxTxTransfer(SPIRxTxMode::WRITE, addr, data); };
    const uint8_t spiReadWord(const uint32_t addr) { return spiRxTxTransfer(SPIRxTxMode::READ, addr); };

    void spiSetClkDivider(void);
    void spiChipSelectEnable(const bool enable) { pdrv->adc_spi_master.ss.set(enable); };
    void spiAutoSlaveSelectEnable(const bool enable) { pdrv->adc_spi_master.controlStatus.ass.set(enable); };

    const bool isSPITransferPending(void) const { return pdrv->adc_spi_master.controlStatus.goBsy.get(); };
    void spiWaitForTransfer(const std::string extraErrMsg = "");

    void enableInterrupt(const bool enable) { pdrv->adc_spi_master.controlStatus.ie.set(enable); };
    void setEndianess(const SPIEndiannes mode) { pdrv->adc_spi_master.controlStatus.lsb.set(mode == SPIEndiannes::MSB); };
    void setMOSILatchEdge(const SPILatchEdge mode) { pdrv->adc_spi_master.controlStatus.txNeg.set(mode == SPILatchEdge::FALLING);};
    void setMISOLatchEdge(const SPILatchEdge mode) { pdrv->adc_spi_master.controlStatus.rxNeg.set(mode == SPILatchEdge::FALLING);};
    void setTestPattern(const LTC218XTestPatters pattern);

    void reset(void);

public:
    /* No need for checking pdrv nullness - driver wrappers "instance()" never returns NULL
     *
     * "inputClkFreq" - default input clock is the wishbone 50MHz clock.
     **/
    explicit LTC218XDriver(rf_adc_ctrl_driver* const pdrv,
                            const uint32_t inputClkFreq = 50000000,
                            const uint32_t spiClkFreq   = 3125000, // 12.5MHz
                            const uint8_t charBitLength = 16);

    LTC218XDriver()                                 = delete;
    LTC218XDriver(const LTC218XDriver& drv)         = delete;
    LTC218XDriver(LTC218XDriver&& drv)              = delete;

    LTC218XDriver& operator=(LTC218XDriver& drv)    = delete;
    LTC218XDriver& operator=(LTC218XDriver&& drv)   = delete;

    /**
     * Test mode allows to force the ADC outputs (D[12:0] lines) to know test pattern value
     **/
    void init(const bool testModeEnabled = false, const uint16_t testPattern = 0x0001);

    /**
     * Pattern test bench for enabled test mode.
     *
     * When chip is configured to operate in the test mode, output ADC channel values should be four times the input pattern
     * (E.g. for test pattern = 0x1, CH0..3 readbacks shall be 0x4).
     *
     * In case of detecting incorrect output value, an exception will be thrown.
     **/
    void runPatternTests();

    ~LTC218XDriver() = default;
}; // class LTC218XDriver


#endif /* SRC_ALLAFCZ_COMMON_LTC218XDRIVER_H_ */
