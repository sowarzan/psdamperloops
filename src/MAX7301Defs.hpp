/*============================================================================*/
/*
 * MAX7301Defs.h
 *
 *  Created on: Oct 20, 2022
 *      Author: sowarzan
 *
 * MAX7301 28-Port I/O Expander
 *
 */
/*============================================================================*/

#ifndef MAX7301DEFS_HPP_
#define MAX7301DEFS_HPP_
#include <stdint.h>
/**
 * Based on the MAX7301 datasheet - page 10:
 * https://datasheets.maximintegrated.com/en/ds/MAX7301.pdf
 */

enum MAX7301RegAddrs: uint8_t
{
    A4_CONFIG_REG_ADDR        = 0x04,
    A6_INPUT_MASK_REG_ADDR    = 0x06,
    A9_PORT_CONFIG_7_4_ADDR   = 0x09,
    AA_PORT_CONFIG_11_8_ADDR  = 0x0A,
    AB_PORT_CONFIG_15_12_ADDR = 0x0B,
    AC_PORT_CONFIG_19_16_ADDR = 0x0C,
    AD_PORT_CONFIG_23_20_ADDR = 0x0D,
    AE_PORT_CONFIG_27_24_ADDR = 0x0E,
    AF_PORT_CONFIG_31_28_ADDR = 0x0F,

    A44_PORT_CONFIG_4_11_ADDR  = 0x44,
    A4C_PORT_CONFIG_12_19_ADDR = 0x4C,
    A54_PORT_CONFIG_20_27_ADDR = 0x54,
    A5C_PORT_CONFIG_28_31_ADDR = 0x5C,
};

enum MAX7301RegBits: uint8_t
{
    A4_SHUTDOWN_CONTROL_BIT    = 0,
    A4_TRANSITION_DET_CTRL_BIT = 7,

    A6_REG_24_BIT              = 0,
    A6_REG_25_BIT              = 1,
    A6_REG_26_BIT              = 2,
    A6_REG_27_BIT              = 3,
    A6_REG_28_BIT              = 4,
    A6_REG_29_BIT              = 5,
    A6_REG_30_BIT              = 6,
};

enum MAX7301RegBitMasks: uint8_t
{
    MAX_CLEAR_REG_MASK             = 0x00,
    MAX_FULL_REG_MASK              = 0xFF,

    A4_NORMAL_OPERATION_MASK       = (1 << A4_SHUTDOWN_CONTROL_BIT),

    A1_TRANSITION_DET_ENABLED_MASK = (1 << A4_TRANSITION_DET_CTRL_BIT),

    A6_REG_24_MASK                 = (1 << A6_REG_24_BIT),
    A6_REG_25_MASK                 = (1 << A6_REG_25_BIT),
    A6_REG_26_MASK                 = (1 << A6_REG_26_BIT),
    A6_REG_27_MASK                 = (1 << A6_REG_27_BIT),
    A6_REG_28_MASK                 = (1 << A6_REG_28_BIT),
    A6_REG_29_MASK                 = (1 << A6_REG_29_BIT),
    A6_REG_30_MASK                 = (1 << A6_REG_30_BIT),

    AA_AF_ALL_OUT_MASK             = 0x55,
    AA_AF_ALL_IN_MASK              = 0xAA,
};

#endif /* MAX7301DEFS_HPP_ */
