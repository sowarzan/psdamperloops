/*============================================================================*/
/**
 * MAX7301 28-Port I/O Expander
 *
 * Based on the MAX7301 datasheet:
 * https://datasheets.maximintegrated.com/en/ds/MAX7301.pdf
 *
 */
/*============================================================================*/

#ifndef MAX7301DRIVER_HPP_
#define MAX7301DRIVER_HPP_
#include "MAX7301Defs.hpp"
#include <unordered_map>
#include <rf-clks_ctrl-lib/rf_clks_ctrl_driver.hpp>
#include <rf-fpintfce_ctrl-lib/rf_fpintfce_ctrl_driver.hpp>

typedef struct MAXConfig
{
     uint32_t inputClkFreq  = 50000000;
     uint32_t spiClkFreq    = 3125000; // 12.5MHz
     uint8_t  charBitLength = 16;
     uint8_t  deviceNumber  = 1;
} MAXConfig;

typedef struct MAX7301Delay
{
    uint16_t DLY0_ADC = MAX_CLEAR_REG_MASK;
    uint16_t DLY1_ADC = MAX_CLEAR_REG_MASK;
    uint16_t DLY2_DAC = MAX_CLEAR_REG_MASK;
    uint16_t DLY3_DAC = MAX_CLEAR_REG_MASK;
} MAX7301Delay;

typedef struct MAX7301Gain
{
    uint8_t SUM1CTRL   = MAX_CLEAR_REG_MASK;
    uint8_t DELTA1CTRL = MAX_CLEAR_REG_MASK;
    uint8_t SUM2CTRL   = MAX_CLEAR_REG_MASK;
    uint8_t DELTA2CTRL = MAX_CLEAR_REG_MASK;
}MAX7301Gain;

template<typename SPIDrvType>
class MAX7301Driver
{
public:
    enum class SPIEndiannes
    {
        LSB,
        MSB
    };

    enum class SPILatchEdge
    {
        FALLING,
        RISING
    };

    enum class SPIRxTxMode
    {
        READ,
        WRITE
    };

private:
    SPIDrvType* const pdrv;
    const MAXConfig cfg;

    /* ===== SPI handlers ===== */
    void spiInit(void);
    void spiStartTransfer(void) const { pdrv->controlStatus.goBsy.set(true); };

    const uint8_t spiRxTxTransfer(const SPIRxTxMode mode, const uint32_t addr, const uint32_t data = 0);
    void spiWriteWord(const uint32_t addr, const uint32_t data) { spiRxTxTransfer(SPIRxTxMode::WRITE, addr, data); };
    const uint8_t spiReadWord(const uint32_t addr) { return spiRxTxTransfer(SPIRxTxMode::READ, addr); };

    void spiSetClkDivider(void);
    void spiChipSelectEnable(const bool enable) { pdrv->ss.set(enable); };
    void spiAutoSlaveSelectEnable(const bool enable) { pdrv->controlStatus.ass.set(enable); };

    const bool isSPITransferPending(void) const { return pdrv->controlStatus.goBsy.get(); };
    void spiWaitForTransfer(const std::string extraErrMsg = "");

    void enableInterrupt(const bool enable) { pdrv->controlStatus.ie.set(enable); };
    void setEndianess(const SPIEndiannes mode) { pdrv->controlStatus.lsb.set(mode == SPIEndiannes::MSB); };
    void setMOSILatchEdge(const SPILatchEdge mode) { pdrv->controlStatus.txNeg.set(mode == SPILatchEdge::FALLING);};
    void setMISOLatchEdge(const SPILatchEdge mode) { pdrv->controlStatus.rxNeg.set(mode == SPILatchEdge::FALLING);};

    void reset(void);

public:
    /* No need for checking pdrv nullness - driver wrappers "instance()" never returns NULL
     *
     * "inputClkFreq" - default input clock is the wishbone 50MHz clock.
     **/
    explicit MAX7301Driver(SPIDrvType* const pdrv, MAXConfig cfg);

    MAX7301Driver()                                 = delete;
    MAX7301Driver(const MAX7301Driver& drv)         = delete;
    MAX7301Driver(MAX7301Driver&& drv)              = delete;

    MAX7301Driver& operator=(MAX7301Driver& drv)    = delete;
    MAX7301Driver& operator=(MAX7301Driver&& drv)   = delete;

    void initGain(MAX7301Gain);
    void initDelay(MAX7301Delay);

    ~MAX7301Driver() = default;
}; // class MAX7301Driver


#include "MAX7301Driver.hxx"

#endif /* MAX7301DRIVER_H_ */
