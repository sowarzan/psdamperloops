/*============================================================================*/
/*
 * MAX7301Driver<SPIDrvType>.cpp
 *
 *  Created on: Oct 20, 2022
 *      Author: sowarzan
 */
/*============================================================================*/

// #include "MAX7301Driver<SPIDrvType>.hpp"


/*============================================================================*/
/* MAX7301 constants & configs */
/*============================================================================*/
static const std::unordered_map<uint32_t, std::string> MAX7301_REGS_AS_STRING = {
        {MAX7301RegAddrs::A4_CONFIG_REG_ADDR,         "A4_CONFIG_REG_ADDR"},
        {MAX7301RegAddrs::A6_INPUT_MASK_REG_ADDR,     "A6_INPUT_MASK_REG_ADDR"},
        {MAX7301RegAddrs::A9_PORT_CONFIG_7_4_ADDR,    "A9_PORT_CONFIG_7_4_ADDR"},
        {MAX7301RegAddrs::AA_PORT_CONFIG_11_8_ADDR,   "AA_PORT_CONFIG_11_8_ADDR"},
        {MAX7301RegAddrs::AB_PORT_CONFIG_15_12_ADDR,  "AB_PORT_CONFIG_15_12_ADDR"},
        {MAX7301RegAddrs::AC_PORT_CONFIG_19_16_ADDR,  "AC_PORT_CONFIG_19_16_ADDR"},
        {MAX7301RegAddrs::AD_PORT_CONFIG_23_20_ADDR,  "AD_PORT_CONFIG_23_20_ADDR"},
        {MAX7301RegAddrs::AE_PORT_CONFIG_27_24_ADDR,  "AE_PORT_CONFIG_27_24_ADDR"},
        {MAX7301RegAddrs::AF_PORT_CONFIG_31_28_ADDR,  "AF_PORT_CONFIG_31_28_ADDR"},
        {MAX7301RegAddrs::A44_PORT_CONFIG_4_11_ADDR,  "A44_PORT_CONFIG_4_11_ADDR"},
        {MAX7301RegAddrs::A4C_PORT_CONFIG_12_19_ADDR, "A4C_PORT_CONFIG_12_19_ADDR"},
        {MAX7301RegAddrs::A54_PORT_CONFIG_20_27_ADDR, "A54_PORT_CONFIG_20_27_ADDR"},
        {MAX7301RegAddrs::A5C_PORT_CONFIG_28_31_ADDR, "A5C_PORT_CONFIG_28_31_ADDR"}
};

/* Maximum possible char's length of an one transfer is 128 bits. */
static constexpr uint8_t SPI_CHARLEN_128b_CODE      = 0x00;
static constexpr uint8_t SPI_CHARLEN_MAX            = 0x80; // 128bit

/**
 * SPI input word format described in the MAX7301 datasheet
 * Page 6, chapter "Port Configuration Map"
 * https://datasheets.maximintegrated.com/en/ds/MAX7301.pdf
 *
 */
static constexpr uint16_t SPI_WORD_RW_BIT           = 15;
static constexpr uint16_t SPI_WORD_READ_MODE_MASK   = (1 << SPI_WORD_RW_BIT);

static constexpr uint16_t SPI_WORD_ADDR_MASK        = 0x7F;     // A[6:0]
static constexpr uint16_t SPI_WORD_ADDR_BITOFFSET   = 8;
static constexpr uint16_t SPI_WORD_DATA_MASK        = 0x00FF;   // D[7:0]

/*============================================================================*/
/* MAX7301 Ctors */
/*============================================================================*/
template <typename SPIDrvType>
MAX7301Driver<SPIDrvType>::MAX7301Driver(SPIDrvType* const pdrv, MAXConfig cfg):
                pdrv(pdrv),
                cfg(cfg)
{
    spiInit();
};

/*============================================================================*/
/* MAX7301 SPI handlers */
/*============================================================================*/
template <typename SPIDrvType>
void MAX7301Driver<SPIDrvType>::spiWaitForTransfer(const std::string extraErrMsg)
{
    uint32_t timeout = 0xFFFF;
    while (isSPITransferPending() and --timeout) {};

    if (not timeout)
    {
        printf("ERROR: MAX7301: writing SPI data failed\n");//) % extraErrMsg;
        throw;
        //LOG_ERROR_IF(logger, msg.str().c_str());
    }
}

template <typename SPIDrvType>
const uint8_t MAX7301Driver<SPIDrvType>::spiRxTxTransfer(const SPIRxTxMode mode, const uint32_t addr, const uint32_t data)
{
    std::array<uint32_t, 2> word;
    uint16_t address = static_cast<uint16_t>( (addr & SPI_WORD_ADDR_MASK) << SPI_WORD_ADDR_BITOFFSET );

    word[0] = static_cast<uint16_t>( address | (data         & SPI_WORD_DATA_MASK) );
    word[1] = static_cast<uint16_t>( address | ((data >> 16) & SPI_WORD_DATA_MASK) );

    if (mode == SPIRxTxMode::READ){
        word[0] = static_cast<uint16_t>(SPI_WORD_READ_MODE_MASK | address);
        word[1] = static_cast<uint16_t>(SPI_WORD_READ_MODE_MASK | address);
    }
    // TODO: currently only Tx0 in use, other lines can be used in future
    word[0] = (word[1] << 16 | word[0]);
    pdrv->transmitReceive0.set(word[0]);

    spiWaitForTransfer("transfer is pending\n");
    printf("transfer is pending\n");
    spiStartTransfer();
    spiWaitForTransfer("bus not responding after start transfer");

    if (mode == SPIRxTxMode::READ)
    {
        uint8_t rxData = static_cast<uint8_t>(pdrv->transmitReceive0.get());
        printf("MAX7301: read word Reg '%s' = 0x%x\n", MAX7301_REGS_AS_STRING.at(addr).c_str(), rxData);
        return rxData;
    }

    // TODO: add mask for work (data only)
    printf("MAX7301: written word Reg '%s' = 0x%x\n", MAX7301_REGS_AS_STRING.at(addr).c_str(), word);
    return 0;
}

template <typename SPIDrvType>
void MAX7301Driver<SPIDrvType>::spiSetClkDivider(void)
{
    /**
     *                (    Input Clock Freq     )
     * divider = floor( -------------------  - 1)
     *                (    2 * SPI Clock Freq   )
     */
    uint16_t div = static_cast<uint16_t>((cfg.inputClkFreq / 2 / cfg.spiClkFreq) - 1);
    pdrv->divider.set(div);
    printf("MAX7301: input clk = %lu, SPI desired clock = %lu, clock divider = %lu\n", cfg.inputClkFreq, cfg.spiClkFreq, div);
}

template <typename SPIDrvType>
void MAX7301Driver<SPIDrvType>::spiInit(void)
{
    printf("MAX7301: initialization DELAY SPI in progress...\n");

    spiSetClkDivider();
    spiAutoSlaveSelectEnable(true);
    spiChipSelectEnable(true);
    enableInterrupt(false);
    setEndianess(SPIEndiannes::LSB);
    setMOSILatchEdge(SPILatchEdge::FALLING);
    setMISOLatchEdge(SPILatchEdge::RISING);

    /**
     * "charLength" = [1bit..127bits] or "SPI_CHARLEN_128b_CODE" for 128bits
     **/
    pdrv->controlStatus.charLen.set(cfg.charBitLength == SPI_CHARLEN_MAX ? SPI_CHARLEN_128b_CODE : cfg.charBitLength);

    printf("MAX7301: initialization DELAY SPI in progress -> done.\n");
}

/*============================================================================*/
/* MAX7301 Initialization handlers */
/*============================================================================*/
template <typename SPIDrvType>
void MAX7301Driver<SPIDrvType>::initGain(MAX7301Gain gain_config)
{

    printf("MAX7301: initialization DELAY in progress...\n");
    spiWriteWord(MAX7301RegAddrs::A4_CONFIG_REG_ADDR,         MAX7301RegBitMasks::A4_NORMAL_OPERATION_MASK);
    spiWriteWord(MAX7301RegAddrs::A9_PORT_CONFIG_7_4_ADDR,    MAX7301RegBitMasks::AA_AF_ALL_OUT_MASK);
    spiWriteWord(MAX7301RegAddrs::AA_PORT_CONFIG_11_8_ADDR,   MAX7301RegBitMasks::AA_AF_ALL_OUT_MASK);
    spiWriteWord(MAX7301RegAddrs::AB_PORT_CONFIG_15_12_ADDR,  MAX7301RegBitMasks::AA_AF_ALL_OUT_MASK);
    spiWriteWord(MAX7301RegAddrs::AC_PORT_CONFIG_19_16_ADDR,  MAX7301RegBitMasks::AA_AF_ALL_OUT_MASK);
    spiWriteWord(MAX7301RegAddrs::AD_PORT_CONFIG_23_20_ADDR,  MAX7301RegBitMasks::AA_AF_ALL_OUT_MASK);
    spiWriteWord(MAX7301RegAddrs::AE_PORT_CONFIG_27_24_ADDR,  MAX7301RegBitMasks::AA_AF_ALL_OUT_MASK);
    spiWriteWord(MAX7301RegAddrs::AF_PORT_CONFIG_31_28_ADDR,  MAX7301RegBitMasks::AA_AF_ALL_OUT_MASK);

    std::array<uint32_t, 3> TxD;
    TxD[0] = ((gain_config.DELTA1CTRL & 0x07) << 5) | (gain_config.SUM1CTRL & 0x1f);
    TxD[1] = ((gain_config.SUM2CTRL & 0x01) << 8)   | ((gain_config.SUM2CTRL & 0x1f) << 2) | ((gain_config.DELTA1CTRL & 0x18));
    TxD[2] = ((gain_config.DELTA2CTRL & 0x1e));
    spiWriteWord(MAX7301RegAddrs::A44_PORT_CONFIG_4_11_ADDR,  TxD[0]);
    spiWriteWord(MAX7301RegAddrs::A4C_PORT_CONFIG_12_19_ADDR, TxD[1]);
    spiWriteWord(MAX7301RegAddrs::A54_PORT_CONFIG_20_27_ADDR, TxD[2]);

    printf("MAX7301: initialization DELAY in progress -> done.\n");
}

template <typename SPIDrvType>
void MAX7301Driver<SPIDrvType>::initDelay(MAX7301Delay delay_config)
{
    printf("MAX7301: initialization DELAY in progress...\n");
    spiWriteWord(MAX7301RegAddrs::A4_CONFIG_REG_ADDR,         MAX7301RegBitMasks::A4_NORMAL_OPERATION_MASK);
    spiWriteWord(MAX7301RegAddrs::A9_PORT_CONFIG_7_4_ADDR,    MAX7301RegBitMasks::AA_AF_ALL_OUT_MASK);
    spiWriteWord(MAX7301RegAddrs::AA_PORT_CONFIG_11_8_ADDR,   MAX7301RegBitMasks::AA_AF_ALL_OUT_MASK);
    spiWriteWord(MAX7301RegAddrs::AB_PORT_CONFIG_15_12_ADDR,  MAX7301RegBitMasks::AA_AF_ALL_OUT_MASK);
    spiWriteWord(MAX7301RegAddrs::AC_PORT_CONFIG_19_16_ADDR,  MAX7301RegBitMasks::AA_AF_ALL_OUT_MASK);
    spiWriteWord(MAX7301RegAddrs::AD_PORT_CONFIG_23_20_ADDR,  MAX7301RegBitMasks::AA_AF_ALL_OUT_MASK);
    spiWriteWord(MAX7301RegAddrs::AE_PORT_CONFIG_27_24_ADDR,  MAX7301RegBitMasks::AA_AF_ALL_OUT_MASK);
    spiWriteWord(MAX7301RegAddrs::AF_PORT_CONFIG_31_28_ADDR,  MAX7301RegBitMasks::AA_AF_ALL_OUT_MASK);

    std::array<uint32_t, 4> TxD;
    TxD[0] = ((delay_config.DLY3_DAC & 0xff) << 16)   | (delay_config.DLY0_ADC & 0xff);
    TxD[1] = ((delay_config.DLY3_DAC & 0x0300) << 8)  | ((delay_config.DLY0_ADC & 0x0300) >> 8);
    TxD[2] = ((delay_config.DLY1_ADC & 0xff) << 16)   | (delay_config.DLY2_DAC & 0xff);
    TxD[3] = ((delay_config.DLY1_ADC & 0x0300) << 8)  | ((delay_config.DLY2_DAC & 0x0300) >> 8);
    spiWriteWord(MAX7301RegAddrs::A44_PORT_CONFIG_4_11_ADDR,  TxD[0]);
    spiWriteWord(MAX7301RegAddrs::A4C_PORT_CONFIG_12_19_ADDR, TxD[1]);
    spiWriteWord(MAX7301RegAddrs::A54_PORT_CONFIG_20_27_ADDR, TxD[2]);
    spiWriteWord(MAX7301RegAddrs::A5C_PORT_CONFIG_28_31_ADDR, TxD[3]);
    spiReadWord(MAX7301RegAddrs::A44_PORT_CONFIG_4_11_ADDR);

    printf("MAX7301: initialization DELAY in progress -> done.\n");
}