#include "ADCDriver.hpp"
#include "MAX7301Driver.hpp"
#include "AD56XXDriver.hpp"
#include "AD9512Driver.hpp"
#include <rf-adc_ctrl-lib/rf_adc_ctrl_driver.hpp>
#include <rf-dac_ctrl-lib/rf_dac_ctrl_driver.hpp>
#include <rf-clks_ctrl-lib/rf_clks_ctrl_driver.hpp>
#include <rf-fpintfce_ctrl-lib/rf_fpintfce_ctrl_driver.hpp>
#include <rf-user_logic-lib/rf_user_logic_driver.hpp>
#include <rf-ps_damper-lib/rf_ps_damper_driver.hpp>
#include <string>
#include <unistd.h>

typedef DriverWrapper::CLKS_CtrlReg::delay_spi_master::Delay_spi_master CLKSSPIDrvType;
typedef DriverWrapper::FPINTFCE_CtrlReg::gain_spi_master::Gain_spi_master FPINTFCESPIDrvType;

int main(int argc, char* argv[])
{

unsigned int microsecond = 1000000;

  printf("Starting...\n");
  rf_adc_ctrl_driver*  pdrv_adc_ctrl           = rf_adc_ctrl_driver::instance("rf_ps_damper",      "board_ctrl_adc",        0, "/user/sowarzan/workspace/pldesign/EDA-02917-V2-0-DamperLoop/MemMap/drivers/rf-ps_damper/rf_ps_damper/");
  rf_clks_ctrl_driver* pdrv_clks_ctrl          = rf_clks_ctrl_driver::instance("rf_ps_damper",     "board_ctrl_clks",       0, "/user/sowarzan/workspace/pldesign/EDA-02917-V2-0-DamperLoop/MemMap/drivers/rf-ps_damper/rf_ps_damper/");
  rf_dac_ctrl_driver* pdrv_dac_ctrl            = rf_dac_ctrl_driver::instance("rf_ps_damper",      "board_ctrl_dac",        0, "/user/sowarzan/workspace/pldesign/EDA-02917-V2-0-DamperLoop/MemMap/drivers/rf-ps_damper/rf_ps_damper/");
  rf_fpintfce_ctrl_driver* pdrv_fpintfce_ctrl  = rf_fpintfce_ctrl_driver::instance("rf_ps_damper", "board_ctrl_fpintfce",   0, "/user/sowarzan/workspace/pldesign/EDA-02917-V2-0-DamperLoop/MemMap/drivers/rf-ps_damper/rf_ps_damper/");
  rf_user_logic_driver* pdrv_user_logic_ctrl   = rf_user_logic_driver::instance("rf_ps_damper",    "board_ctrl_user_logic", 0, "/user/sowarzan/workspace/pldesign/EDA-02917-V2-0-DamperLoop/MemMap/drivers/rf-ps_damper/rf_ps_damper/");
  rf_ps_damper_driver* pdrv                    = rf_ps_damper_driver::instance("rf_ps_damper",     "psDamperLoops",         0, "/user/sowarzan/workspace/pldesign/EDA-02917-V2-0-DamperLoop/MemMap/drivers/rf-ps_damper/rf_ps_damper/");

  printf("DATE from register is %d\n", pdrv->board_ctrl.firmwareVersion.get() );

  MAX7301Gain  gain_config
  {
    MAX_FULL_REG_MASK, // SUM1CTRL
    MAX_FULL_REG_MASK, // DELTA1CTRL
    MAX_FULL_REG_MASK, // SUM2CTRL
    MAX_FULL_REG_MASK  // DELTA2CTRL
  };

  MAX7301Delay delay_config
  {
      625, // DLY0_ADC 50% -> <450-800> -> 625
      0,   // DLY1_ADC
      270, // DLY2_DAC 50% -> <40-500> -> 270
      0    // DLY3_DAC
  };

  MAXConfig ps_delay;
  ps_delay.deviceNumber = 2;
  MAXConfig ps_gain;

  MAX7301Driver<CLKSSPIDrvType> delayDrv(&pdrv_clks_ctrl->delay_spi_master, ps_delay);
  delayDrv.initDelay(delay_config);
  pdrv_clks_ctrl->control.pha_sel.set(0);
  pdrv_clks_ctrl->control.pha_sel.set(1);

  MAX7301Driver<FPINTFCESPIDrvType>  gainDrv(&pdrv_fpintfce_ctrl->gain_spi_master, ps_gain);
  gainDrv.initGain(gain_config);
  ADCDriver   adcDrv(pdrv_adc_ctrl);

  ADCConfig ps_ltc;
  ps_ltc.clkSrc = ADC_CLK_SRC_FPCLK_0_ONLY;
  ps_ltc.outaInputDelays = {0, 0, 0, 0};
  ps_ltc.testsEnabled = false;

  adcDrv.init(ps_ltc);
  //adcDrv.patternTest(ps_ltc);


  AD56XXDriver   dacDrv(pdrv_dac_ctrl);
  dacDrv.init();

  AD9512Driver   clkDrv(pdrv_clks_ctrl);
  clkDrv.init();

  pdrv_user_logic_ctrl -> control.set(3);
  printf("USER_LOGIC output setter is: %d\n", pdrv_user_logic_ctrl -> control.get() );
  return 0;
}